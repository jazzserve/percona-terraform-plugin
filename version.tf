terraform {
  required_providers {
    percona = {
      version = "~> 1.0.0"
      source  = "terraform-percona.com/terraform-percona/percona"
    }
  }
}