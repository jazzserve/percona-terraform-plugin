package ptr

func Str(str string) *string {
	return &str
}
